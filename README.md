Toppyt is a lightweight Task-Oriented Programming (TOP) framework for Python web applications. It let's you create ASGI web applications in a task-oriented style that can be served by any ASGI compatible web server.

Toppyt is based on the iTasks framework in the Clean programming language, but is intended to be simpler and smaller. 
